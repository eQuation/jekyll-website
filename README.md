# Resume Website

Website containing information about John Mollberg's professional career.

## Getting Started

### Before You Start

This website was created using [Jekyll](https://jekyllrb.com/). The specific
theme used is called
[Orbit Theme](https://themes.3rdwavemedia.com/bootstrap-templates/resume/orbit-free-resume-cv-bootstrap-theme-for-developers/)
which was implemented in
[online cv jekyll](https://github.com/sharu725/online-cv/), with some
modifications. I don't know why you would clone this project, use theirs
instead. If you choose to ignore my suggestion, carry on.

### Installation

1. [Install Jekyll.](https://jekyllrb.com/)
1. Clone this repository.

### Building & Running

1. `cd` into the project directory.
1. Run `jekyll serve`. The website will be built in `_site/` and will now be
available at
[http://localhost:4000/public/index.html](http://localhost:4000/public/index.html)

## Deployment

Deployment of this project happens automatically through
[Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/). It uses the
[Ruby](https://hub.docker.com/_/ruby) [Docker](https://www.docker.com) image for
building and the [Python](https://hub.docker.com/_/python) image for deploying
to an [AWS](https://aws.amazon.com/) [S3](https://aws.amazon.com/s3) bucket.

Note: In addition to uploading the generated `public` folder to S3, the file
located at [/support/index.html](/support/index.html) is also uploaded.

## Tools, Libraries, & Frameworks

* [Jekyll](https://jekyllrb.com/)
* [online cv Jekyll](https://github.com/sharu725/online-cv)
* [Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/)

## Author

* **John Mollberg** - *Sole author*

## License

Future distribution of this project requires source code changes, as the Orbit
theme that is used for styling is licensed under the Creative Commons BY 3.0
License. Please consult the [LICENSE.md](LICENSE.md#Orbit-Theme-License) for further instructions.

The source code changes required can be ignored (though not the licensing
requirements) if you purchase the license to the theme
[here](https://themes.3rdwavemedia.com/bootstrap-templates/resume/orbit-free-resume-cv-bootstrap-theme-for-developers).

The original Orbit Theme license can be found
[here](INTERNAL-LICENSES.md#Orbit-Theme).

This project uses [online cv Jekyll](https://github.com/sharu725/online-cv), the
license of which can be found [here](INTERNAL-LICENSES.md#online-cv-jekyll)
