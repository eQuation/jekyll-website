# License

## Orbit Theme License

In accordance with the Creative Commons BY 3.0 License, parts of this project
are prohibited from redistribution unless the following two clauses are
enacted:

1. The following HTML element must replace the copyright HTML at the bottom of
the footer.html file located in the root of the project. Therefore, at the
bottom of the index.html file that is produced by this project, the same HTML
is written:

```
<small class="copyright">Designed with <i class="fa fa-heart"></i> by
  <a href="http://themes.3rdwavemedia.com" target="_blank" rel="nofollow">
    Xiaoying Riley
  </a>
</small>
```

2. Redistribution of this work must include all text including this in the
section labeled "Orbit Theme License" in a file named LICENSE.md or LICENSE at
the root of the project.

Given that these two conditions are met, the remainder of the project is
licensed under the MIT License:

# MIT License

Copyright (c) 2018 John Mollberg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
